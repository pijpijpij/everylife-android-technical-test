/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.usecase

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.pij.everylife.repository.TaskRepository
import io.reactivex.Completable
import io.reactivex.Observable
import org.junit.jupiter.api.Test

internal class OfflineTasksLoaderTest {

    private fun silentTaskRepository() =
        mock<TaskRepository> { on { tasks() } doReturn Observable.never() }

    private fun silentAsyncTasksLoader() =
        mock<AsyncTasksLoader> { on { startDownload() } doReturn Completable.never() }

    @Test
    fun `Refresh not triggered on construction`() {
        // given
        val online = silentAsyncTasksLoader()
        val offline = silentTaskRepository()

        // when
        OfflineTasksLoader(offline, online)

        // then
        verify(online, never()).startDownload()
    }

    @Test
    fun `Refresh triggered on subscription`() {
        // given
        val online = silentAsyncTasksLoader()
        val offline = silentTaskRepository()
        val sut = OfflineTasksLoader(offline, online)

        // when
        sut.tasks().test()

        // then
        verify(online).startDownload()
    }

}