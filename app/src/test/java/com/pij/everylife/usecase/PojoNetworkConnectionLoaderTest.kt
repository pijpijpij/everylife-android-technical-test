/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.usecase

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.pij.everylife.repository.NetworkMonitor
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.jupiter.api.Test

internal class PojoNetworkConnectionLoaderTest {

    @Test
    fun `connectionStatus provides current status then status changes`() {
        // given
        val repository =
            mock<NetworkMonitor> {
                on { currentStatus } doReturn Single.just(true)
                on { statusChanges } doReturn Observable.just(true, false)
            }
        val sut = PojoNetworkConnectionLoader(repository)

        // when
        val result = sut.connectionStatus().test()

        // then
        result.assertResult(true, true, false)
    }

    @Test
    fun `Repository not queried at construction`() {
        // given
        val repository = mock<NetworkMonitor> {
            on { currentStatus } doReturn Single.error(UnsupportedOperationException("ta da"))
            on { statusChanges } doReturn Observable.error(UnsupportedOperationException("ta da"))
        }

        // when
        val sut = PojoNetworkConnectionLoader(repository)

        // then
        verify(repository, never()).currentStatus
        verify(repository, never()).statusChanges
    }

}