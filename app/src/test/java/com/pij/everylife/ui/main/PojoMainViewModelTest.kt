/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.ui.main

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.pij.everylife.entity.DatedTask
import com.pij.everylife.usecase.NetworkConnectionLoader
import com.pij.everylife.usecase.TasksLoader
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

internal class PojoMainViewModelTest {

    private fun silentNetworkConnectionLoader() = mock<NetworkConnectionLoader> {
        on { connectionStatus() } doReturn Observable.never()
    }

    private fun silentTasksLoader() = mock<TasksLoader> {
        on { tasks() } doReturn Observable.never()
    }

    @Test
    fun `Emits Disconnected and no task first`() {
        // given
        val connectionLoader = silentNetworkConnectionLoader()
        val tasksLoader = silentTasksLoader()
        val sut = PojoMainViewModel(connectionLoader, tasksLoader)

        // when
        val result = sut.model.test()

        // then
        result.assertNotComplete()
            .assertValueCount(1)
    }

    @ParameterizedTest
    @ValueSource(booleans = [false, true])
    fun `Emits Model when connectionLoader emits`(connection: Boolean) {
        // given
        val connections = PublishSubject.create<Boolean>()
        val connectionLoader = mock<NetworkConnectionLoader> {
            on { connectionStatus() } doReturn connections.hide()
        }
        val tasksLoader = silentTasksLoader()
        val sut = PojoMainViewModel(connectionLoader, tasksLoader)
        val result = sut.model.skip(1).test()

        // when
        connections.onNext(connection)

        // then
        result.assertNotComplete()
            .assertValue { it.showOffline == !connection }

    }

    @Test
    fun `Emits Model when tasksLoader emits`() {
        // given
        val connectionLoader = silentNetworkConnectionLoader()
        val tasks = PublishSubject.create<List<DatedTask>>()
        val tasksLoader = mock<TasksLoader> { on { tasks() } doReturn tasks.hide() }
        val sut = PojoMainViewModel(connectionLoader, tasksLoader)
        val result = sut.model.skip(1).test()

        // when
        tasks.onNext(listOf())

        // then
        result.assertNotComplete()
            .assertValueCount(1)
    }
}