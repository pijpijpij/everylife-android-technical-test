/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.repository.room

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.time.Instant
import kotlin.random.Random

@RunWith(AndroidJUnit4::class)
class DatedTaskDAORoomTest {

    private lateinit var database: Database
    private lateinit var dao: DatedTaskDAO

    private fun createDatedTask() = DatedTaskDTO(
        id = Random.nextLong(),
        age = Instant.now(),
        type = DatedTaskDTO.Type.NUTRITION,
        description = "whatever",
        title = "some text"
    )

    @Before
    fun setupDatabase() {
        database = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getInstrumentation().context,
            Database::class.java
        )
            .build()

        dao = database.datedTaskDao()
    }

    @After
    fun teardownDatabase() {
        database.close()
    }

    @Test
    fun noSave_databaseHasNoDatedTask() {
        // given

        // when
        val result = dao.tasks().blockingFirst()

        // then
        assertEquals(0, result.size)
    }

    @Test
    fun save1DatedTask_databaseHasDatedTask() {
        // given
        val datedTask = createDatedTask()

        // when
        dao.save(datedTask).blockingAwait()

        // then
        val result = dao.tasks().blockingFirst()
        assertEquals(1, result.size)
    }


    @Test
    fun saveDatedTaskTwice_databaseHasLatestDatedTask() {
        // given
        val first = createDatedTask().copy(id=1L, title = "first")
        val second = first.copy(title = "second")

        // when
        dao.save(first).blockingAwait()
        dao.save(second).blockingAwait()

        // then
        val result = dao.tasks().blockingFirst()
        assertThat(result, hasSize(1))
        assertThat(result[0].title, equalTo("second"))
    }


}