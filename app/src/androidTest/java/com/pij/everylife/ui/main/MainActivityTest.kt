/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.ui.main

import androidx.test.espresso.intent.rule.IntentsTestRule
import com.pij.everylife.entity.Task
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject

@HiltAndroidTest
class MainActivityTest {

    @get:Rule(order = 0)
    val hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    val activity = IntentsTestRule(MainActivity::class.java, false, false)

    @Inject
    internal lateinit var robot: MainRobot

    @Before
    fun setUp() {
        hiltRule.inject()
    }

    @Test
    fun test_start_empty_database_shows_empty_text() {
        robot.given(activity) {

        } `when` {
            launchScreen()

        } then {
            checkTextFieldIsVisible("No tasks available")
        }
    }

    @Test
    fun test_start_empty_database_no_network_shows_No_internet_connection_banner() {
        robot.given(activity) {

            disconnectNetwork()

        } `when` {
            launchScreen()

        } then {
            checkTextFieldIsVisible("No internet connection")
        }
    }

    @Test
    fun test_start_1_task_no_network_shows_No_internet_connection_banner() {
        robot.given(activity) {

            disconnectNetwork()
            insertDatedTask(1, "a title", "a description", Task.Type.NUTRITION)

        } `when` {
            launchScreen()

        } then {
            checkTextFieldIsVisible("No internet connection")
        }
    }

    @Test
    fun test_start_1_task_no_network_shows_task_text_fields() {
        robot.given(activity) {

            disconnectNetwork()
            insertDatedTask(1, "a title", "a description", Task.Type.NUTRITION)

        } `when` {
            launchScreen()

        } then {
            taskIsListed("a title", "a description")
        }
    }


}