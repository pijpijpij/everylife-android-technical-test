/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.ui.main

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.scrollTo
import androidx.test.espresso.matcher.ViewMatchers.*
import com.pij.everylife.R
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Matcher

class MainResult {

    fun checkTextFieldIsVisible(text: String) {
        checkIsDisplayed(withText(text))
    }

    fun taskIsListed(title: String, description: String) {
        onView(mainList()).perform(scrollTo<MainAdapter.MainViewHolder>(task(title, description)))
    }

    private fun checkIsDisplayed(matcher: Matcher<View>) {
        onView(matcher).check(matches(isCompletelyDisplayed()))
    }

    private fun mainPanel(): Matcher<View> = withId(R.id.main)
    private fun mainList(): Matcher<View> = allOf(isDescendantOfA(mainPanel()), withId(R.id.list))

    private fun task(title: String, description: String): Matcher<View> = allOf(
        hasDescendant(withText(title)),
        hasDescendant(withText(description))
    )

}
