/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.ui.main

import androidx.test.rule.ActivityTestRule
import com.pij.everylife.entity.DatedTask
import com.pij.everylife.entity.Task
import com.pij.everylife.usecase.SubjectNetworkConnectionLoader
import com.pij.everylife.usecase.SubjectTasksLoader
import java.time.Instant
import javax.inject.Inject

class MainRobot @Inject constructor(
    private val networkConnectionLoader: SubjectNetworkConnectionLoader,
    private val tasksLoader: SubjectTasksLoader
) {

    private lateinit var activity: ActivityTestRule<MainActivity>

    fun given(
        activity: ActivityTestRule<MainActivity>,
        givenBlock: MainRobot.() -> Unit
    ): MainRobot {
        this.activity = activity
        givenBlock()
        return this
    }

    fun disconnectNetwork() = networkConnectionLoader.connectionStatus(false)


    fun launchScreen() {
        activity.launchActivity(null)
    }

    infix fun `when`(whenBlock: MainRobot.() -> Unit): MainRobot = apply {
        whenBlock()
    }

    infix fun then(verify: MainResult.() -> Unit): MainResult =
        MainResult().apply {
            verify()
        }

    private var tasks: List<DatedTask> = emptyList()
    fun insertDatedTask(id: Long, title: String, description: String, type: Task.Type) {
        tasks = tasks + DatedTask(
            age = Instant.now(),
            task = Task(
                id = id,
                title = title,
                description = description,
                type = type
            )
        )
        tasksLoader.tasks(tasks)
    }
}