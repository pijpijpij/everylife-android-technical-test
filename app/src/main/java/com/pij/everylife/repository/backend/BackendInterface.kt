/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.repository.backend

import io.reactivex.Single
import retrofit2.http.GET

interface BackendInterface {

    @GET("tasks.json")
    fun tasks(): Single<List<TaskDTO>>

    data class TaskDTO(val id: Long, val name: String, val description: String, val type: Type) {
        @Suppress("EnumEntryName")
        enum class Type { general, hydration, medication, nutrition }
    }
}