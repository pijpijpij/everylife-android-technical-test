/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.repository.room

import com.pij.everylife.entity.DatedTask
import com.pij.everylife.entity.Task
import com.pij.everylife.repository.TaskRepository
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class RoomTaskRepository @Inject constructor(private val dao: DatedTaskDAO) : TaskRepository {

    override fun tasks(): Observable<List<DatedTask>> = dao.tasks()
        .map { list -> list.map { it.toDomain() } }

    override fun task(input: DatedTask): Completable = Single.just(input)
        .map { it.fromDomain() }
        .flatMapCompletable { dao.save(it) }


    private fun DatedTaskDTO.toDomain() = DatedTask(
        age = age,
        task = Task(
            id = id,
            title = title,
            description = description,
            type = type.toDomain()
        )
    )

    private fun DatedTaskDTO.Type.toDomain() = when (this) {
        DatedTaskDTO.Type.GENERAL -> Task.Type.GENERAL
        DatedTaskDTO.Type.MEDICATION -> Task.Type.MEDICATION
        DatedTaskDTO.Type.HYDRATION -> Task.Type.HYDRATION
        DatedTaskDTO.Type.NUTRITION -> Task.Type.NUTRITION
    }

    private fun DatedTask.fromDomain() = DatedTaskDTO(
        age = age,
        id = task.id,
        title = task.title,
        description = task.description,
        type = task.type.fromDomain()
    )

    private fun Task.Type.fromDomain() = when (this) {
        Task.Type.GENERAL -> DatedTaskDTO.Type.GENERAL
        Task.Type.MEDICATION -> DatedTaskDTO.Type.MEDICATION
        Task.Type.HYDRATION -> DatedTaskDTO.Type.HYDRATION
        Task.Type.NUTRITION -> DatedTaskDTO.Type.NUTRITION
    }
}
