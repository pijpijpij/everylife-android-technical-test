/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.repository.android

import android.net.ConnectivityManager
import android.net.Network
import com.pij.everylife.repository.NetworkMonitor
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.Single
import javax.inject.Inject

internal class AndroidNetworkMonitor @Inject constructor(private val connectivityManager: ConnectivityManager) :
    NetworkMonitor {

    override val statusChanges: Observable<Boolean> = Observable.create { emitter ->
        connectivityManager.registerDefaultNetworkCallback(CallbackToEmitter(emitter))
    }

    override val currentStatus: Single<Boolean>
        get() = Single.fromCallable { connectivityManager.activeNetwork != null }

    private class CallbackToEmitter(private val emitter: ObservableEmitter<Boolean>) :
        ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            if (!emitter.isDisposed) emitter.onNext(true)
        }

        override fun onLost(network: Network) {
            if (!emitter.isDisposed) emitter.onNext(false)
        }

    }

}