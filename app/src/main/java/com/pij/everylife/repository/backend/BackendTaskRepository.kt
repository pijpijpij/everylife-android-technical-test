/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.repository.backend

import com.pij.everylife.entity.DatedTask
import com.pij.everylife.entity.Task
import com.pij.everylife.repository.TaskRepository
import io.reactivex.Completable
import io.reactivex.Observable
import java.time.Instant
import javax.inject.Inject

class BackendTaskRepository @Inject constructor(private val backend: BackendInterface) : TaskRepository {

    override fun tasks(): Observable<List<DatedTask>> = backend.tasks()
        .map { list ->
            val age = Instant.now()
            list.map { it.toDomain() }.map { DatedTask(age, it) }
        }
        .toObservable()

    override fun task(input: DatedTask): Completable =
        Completable.error(UnsupportedOperationException("Cannot change the tasks on the back-end"))

    private fun BackendInterface.TaskDTO.toDomain() = Task(
        id = id,
        title = name,
        description = description,
        type = type.toDomain(),
    )

    private fun BackendInterface.TaskDTO.Type.toDomain() = when (this) {
        BackendInterface.TaskDTO.Type.general -> Task.Type.GENERAL
        BackendInterface.TaskDTO.Type.hydration -> Task.Type.HYDRATION
        BackendInterface.TaskDTO.Type.medication -> Task.Type.MEDICATION
        BackendInterface.TaskDTO.Type.nutrition -> Task.Type.NUTRITION
    }
}