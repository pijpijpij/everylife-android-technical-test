/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.repository.room

import androidx.room.TypeConverter
import java.time.Instant

class Converters {
    @TypeConverter
    fun instantFromSQL(input: Long): Instant = Instant.ofEpochMilli(input)

    @TypeConverter
    fun instantToSQL(input: Instant): Long = input.toEpochMilli()

    @TypeConverter
    fun fromSQL(input: String): DatedTaskDTO.Type = DatedTaskDTO.Type.valueOf(input)

    @TypeConverter
    fun toSQL(input: DatedTaskDTO.Type): String = input.name

}
