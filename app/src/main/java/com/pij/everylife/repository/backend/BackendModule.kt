/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.repository.backend

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Provides repository implementation the rest of the app can use.
 */
@Module
@InstallIn(SingletonComponent::class)
internal class BackendModule {

    @Provides
    internal fun providesOkHttpClient() =
        with(OkHttpClient.Builder()) {
            connectTimeout(180, TimeUnit.SECONDS)
            writeTimeout(180, TimeUnit.SECONDS)
            readTimeout(180, TimeUnit.SECONDS)

            addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })

            build()
        }

    @Provides
    @Singleton
    internal fun providesRetrofit(client: OkHttpClient, moshi: Moshi): Retrofit =
        with(Retrofit.Builder()) {
            baseUrl("https://adam-deleteme.s3.amazonaws.com/")
            client(client)
            addConverterFactory(MoshiConverterFactory.create(moshi))
            addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            build()
        }


    @Provides
    internal fun providesMoshi(): Moshi = with(Moshi.Builder()) {
        add(KotlinJsonAdapterFactory())
        build()
    }


    @Provides
    internal fun providesBackendInterface(retrofit: Retrofit): BackendInterface =
        retrofit.create(BackendInterface::class.java)

}

