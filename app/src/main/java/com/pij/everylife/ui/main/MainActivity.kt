/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.ui.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.pij.everylife.databinding.MainActivityBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {


    @Inject
    internal lateinit var adapter: MainAdapter

    private val androidViewModel by viewModels<MainAndroidViewModel>()
    private lateinit var binding: MainActivityBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.list.adapter = adapter

        androidViewModel.model.observe(this, { display(it) })
    }

    private fun display(model: MainViewModel.MainModel) {
        with(model.showOffline) {
            if (this != binding.offline.isVisible) binding.offline.isVisible = this
        }
        with(model.tasks) {
            binding.empty.isVisible = isEmpty()
            binding.list.isVisible = model.tasks.isNotEmpty()
            adapter.items = model.tasks
        }
    }

}