/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pij.everylife.R
import com.pij.everylife.databinding.MainListItemBinding
import javax.inject.Inject

class MainAdapter @Inject constructor() : RecyclerView.Adapter<MainAdapter.MainViewHolder>() {

    data class Option(val label: Int, val hideDrawables: Boolean, val action: () -> Unit) {
        constructor(label: Int, action: () -> Unit) : this(label, false, action)
    }

    var items: List<MainViewModel.MainModel.Task> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = MainListItemBinding.inflate(layoutInflater, parent, false)
        return MainViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class MainViewHolder(private val binding: MainListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(task: MainViewModel.MainModel.Task) {
            binding.title.text = task.title
            binding.description.text = task.description
            binding.type.setImageResource(
                when (task.type) {
                    MainViewModel.MainModel.Task.Type.GENERAL -> R.drawable.general
                    MainViewModel.MainModel.Task.Type.MEDICATION -> R.drawable.medication
                    MainViewModel.MainModel.Task.Type.HYDRATION -> R.drawable.hydration
                    MainViewModel.MainModel.Task.Type.NUTRITION -> R.drawable.nutrition
                }
            )
        }
    }
}