/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.ui.main

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.pij.everylife.usecase.NetworkConnectionLoader
import com.pij.everylife.usecase.TasksLoader
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ActivityContext

@Module
@InstallIn(ViewModelComponent::class)
class MainModule {

    @Provides
    internal fun provideMainActivity(@ActivityContext activity: Context) = activity as MainActivity

    @Provides
    internal fun provideMainViewModel(
        networkConnectionLoader: NetworkConnectionLoader,
        tasksLoader: TasksLoader
    ): MainViewModel =
        PojoMainViewModel(networkConnectionLoader, tasksLoader)
}