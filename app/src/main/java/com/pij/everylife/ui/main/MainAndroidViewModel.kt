/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.scopes.ActivityScoped
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import javax.inject.Inject

@HiltViewModel
class MainAndroidViewModel @Inject constructor(private val delegate: MainViewModel) :
    ViewModel() {

    private val subscriptions = CompositeDisposable()
    private val liveData = MutableLiveData<MainViewModel.MainModel>();

    val model: LiveData<MainViewModel.MainModel>
        get() = liveData


    init {
        subscriptions += delegate.model
            .subscribe(liveData::postValue)
            { Log.e(this::class.toString(), "View is now DITW", it) }
    }

    override fun onCleared() {
        subscriptions.clear()
    }
}