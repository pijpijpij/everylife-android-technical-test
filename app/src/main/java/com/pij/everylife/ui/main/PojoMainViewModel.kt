/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.ui.main

import android.util.Log
import com.pij.everylife.entity.DatedTask
import com.pij.everylife.entity.Task
import com.pij.everylife.usecase.NetworkConnectionLoader
import com.pij.everylife.usecase.TasksLoader
import io.reactivex.Observable
import javax.inject.Inject

class PojoMainViewModel @Inject constructor(
    private val networkConnectionLoader: NetworkConnectionLoader,
    private val tasksLoader: TasksLoader
) : MainViewModel {

    override val model: Observable<MainViewModel.MainModel>
        get() = Observable.combineLatest(
            networkConnectionLoader.connectionStatus().startWith(true)
                .doOnNext { Log.d("PJC", "connection status is $it") },
            Observable.concat(
                Observable.just<List<DatedTask>>(emptyList()),
                tasksLoader.tasks()
            ).map { list -> list.map { it.fromDomain() } }
        )
        { connected, tasks -> MainViewModel.MainModel(!connected, tasks) }
            .doOnNext { Log.d("PJC", "$it") }

    private fun DatedTask.fromDomain() = MainViewModel.MainModel.Task(
        title = task.title,
        description = task.description,
        type = task.type.fromDomain()
    )

    private fun Task.Type.fromDomain() = when (this) {
        Task.Type.GENERAL -> MainViewModel.MainModel.Task.Type.GENERAL
        Task.Type.MEDICATION -> MainViewModel.MainModel.Task.Type.MEDICATION
        Task.Type.HYDRATION -> MainViewModel.MainModel.Task.Type.HYDRATION
        Task.Type.NUTRITION -> MainViewModel.MainModel.Task.Type.NUTRITION
    }
}

