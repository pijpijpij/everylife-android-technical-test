/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.ui.main

import io.reactivex.Observable
import java.time.Instant


interface MainViewModel {

    val model: Observable<MainModel>

    data class MainModel(val showOffline: Boolean, val tasks: List<Task>) {
        data class Task(val type: Type, val title: String, val description: String) {
            enum class Type { GENERAL, MEDICATION, HYDRATION, NUTRITION }
        }

    }
}
