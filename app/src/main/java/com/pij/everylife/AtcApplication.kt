/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife

import android.app.Application
import androidx.work.Configuration
import androidx.work.WorkManager
import androidx.work.WorkRequest
import dagger.Lazy
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class AtcApplication : Application(), Configuration.Provider {

    override fun onCreate() {
        super.onCreate()
        initialiseWorkManager()
    }

    @Inject
    internal lateinit var injectedWorkManagerConfiguration: Lazy<Configuration>

    @Inject
    internal lateinit var injectedWorkManager: Lazy<WorkManager>

    @Inject
    internal lateinit var workRequests: Set<@JvmSuppressWildcards WorkRequest>

    private fun initialiseWorkManager() {
        workRequests.forEach { injectedWorkManager.get().enqueue(it) }
    }

    override fun getWorkManagerConfiguration(): Configuration =
        injectedWorkManagerConfiguration.get()

}