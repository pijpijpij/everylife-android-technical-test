/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.work

import android.content.Context
import androidx.work.RxWorker
import androidx.work.WorkerParameters
import com.pij.everylife.repository.TaskRepository
import io.reactivex.Single
import javax.inject.Inject

class TaskLoadingWorker(
    context: Context,
    workerParams: WorkerParameters,
    private val upToDate: TaskRepository,
    private val outdated: TaskRepository
) : RxWorker(context, workerParams) {

    override fun createWork(): Single<Result> {
        return upToDate.tasks()
            .flatMapIterable { it }
            .flatMapCompletable(outdated::task)
            .toSingleDefault(Result.success())
            .onErrorReturn { Result.failure() }
    }
}