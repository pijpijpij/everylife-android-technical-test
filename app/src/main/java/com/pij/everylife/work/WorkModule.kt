/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.work

import androidx.work.WorkManager
import androidx.work.WorkerFactory
import com.pij.everylife.repository.backend.BackendTaskRepository
import com.pij.everylife.repository.room.RoomTaskRepository
import com.pij.everylife.usecase.AsyncTasksLoader
import dagger.Lazy
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoSet

/**
 * Provides WorkManager-level objects.
 */
@Module
@InstallIn(SingletonComponent::class)
internal class WorkModule {

    @Provides
    @IntoSet
    fun provideTaskLoadingWorkerFactory(
        upToDate: BackendTaskRepository,
        outdated: RoomTaskRepository
    ): WorkerFactory = TaskLoadingWorkerFactory(upToDate, outdated)

    @Provides
    fun provideAsyncTasksLoader(workManager: Lazy<WorkManager>): AsyncTasksLoader =
        WorkAsyncTasksLoader(workManager)

}

