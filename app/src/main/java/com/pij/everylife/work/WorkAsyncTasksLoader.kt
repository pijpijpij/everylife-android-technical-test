/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.work

import androidx.work.Constraints
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.pij.everylife.usecase.AsyncTasksLoader
import dagger.Lazy
import io.reactivex.Completable
import javax.inject.Inject

class WorkAsyncTasksLoader @Inject constructor(private val workManager: Lazy<WorkManager>) :
    AsyncTasksLoader {

    /** Create a new WorkRequest and stick it in the WorkManager queue. */
    override fun startDownload() = Completable.fromAction {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val worker = OneTimeWorkRequest.Builder(TaskLoadingWorker::class.java)
            .setConstraints(constraints)
            .build()

        workManager.get().enqueue(worker)
    }

}