/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.usecase

import android.util.Log
import com.pij.everylife.entity.DatedTask
import com.pij.everylife.repository.TaskRepository
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class OfflineTasksLoader @Inject constructor(
    private val offline: TaskRepository,
    private val online: AsyncTasksLoader
) : TasksLoader {

    override fun tasks(): Observable<List<DatedTask>> = online.startDownload()
            .doOnError { Log.w(this::class.java.simpleName, "Failed to fetch online tasks", it) }
            .onErrorComplete()
            .andThen(offline.tasks())
            .subscribeOn(Schedulers.io())
}