/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife.usecase

import com.pij.everylife.repository.room.RoomTaskRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * Provides global use cases the rest of the app can use.
 */
@Module
@InstallIn(SingletonComponent::class)
internal class UsecaseModule {

    @Provides
    internal fun provideNetworkConnectionLoader(implementation: PojoNetworkConnectionLoader): NetworkConnectionLoader =
        implementation

    @Provides
    internal fun provideTasksLoader(
        offline: RoomTaskRepository,
        online: AsyncTasksLoader
    ): TasksLoader =
        OfflineTasksLoader(offline, online)

}

