/*
 * Copyright (c) 2021, Chiswick Forest
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *          http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and limitations under the License.
 *
 */

package com.pij.everylife

import android.content.Context
import androidx.work.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.ElementsIntoSet
import javax.inject.Singleton

/**
 * Provides WorkManager-level objects.
 */
@Module
@InstallIn(SingletonComponent::class)
internal class WorkManagerModule {

    @Provides
    fun providesWorkManagerConfiguration(workerFactories: Set<@JvmSuppressWildcards WorkerFactory>) =
        Configuration.Builder()
            .setWorkerFactory(DelegatingWorkerFactory().apply {
                workerFactories.forEach {
                    addFactory(
                        it
                    )
                }
            })
            .build()

    @Singleton
    @Provides
    fun providesWorkManager(application: Context) = WorkManager.getInstance(application)

    /** The only point of this method is to allow the app to build when we provide no Worker factory. */
    @Provides
    @ElementsIntoSet
    fun providesEmptySetOfWorkerFactories() = emptySet<@JvmSuppressWildcards WorkerFactory>()

    /** The only point of this method is to allow the app to build when we provide no Worker request. */
    @Provides
    @ElementsIntoSet
    internal fun providesEmptySetOfWorkerRequest() = emptySet<@JvmSuppressWildcards WorkRequest>()


}

